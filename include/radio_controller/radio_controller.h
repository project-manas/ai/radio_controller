#ifndef CONTROLLER_H
#define CONTROLLER_H

#include "ros/ros.h"
#include "serial/serial.h"
#include "serial_parser/serial_parser.h"

namespace radio_controller {
class RadioController {
 private:
  ros::NodeHandle *nh_;
  ros::NodeHandle private_nh_;
  serial::Serial *serial_;
  serial::SerialParser *serial_parser;

  double rate_;
  int data_size_;
  double max_angular_velocity_;
  double max_linear_velocity_;
  double min_angular_velocity_;
  double min_linear_velocity_;
  std::string start_delimitter_;
  std::string end_delimitter_;
  std::string pub_topic_;

 public:
  RadioController(ros::NodeHandle *node, serial::Serial *serial);
  void run();
};
}
#endif
