#include "radio_controller/radio_controller.h"
#include <geometry_msgs/Twist.h>
#include "ros/ros.h"
#include "serial/serial.h"
#include <std_msgs/Float64.h>
#include <geometry_msgs/Twist.h>
#include <iostream>

#define input_range 8.0
#define wheel_dist  0.718
#define wheel_radius 0.18

using namespace radio_controller;

float map(int input, float max, float min) {
  return (min + (max - min) * ((float)input - 0) / (input_range)) * -1;
}
RadioController::RadioController(ros::NodeHandle *node, serial::Serial *serial)
    : nh_(node), private_nh_("~"), serial_(serial) {
  private_nh_.param("rate", rate_, 30.0);
  private_nh_.param("data_size", data_size_, 2);
  private_nh_.param("max_linear_velocity", max_linear_velocity_, 2.0);
  private_nh_.param("max_angular_velocity", max_angular_velocity_, 2.0);
  private_nh_.param("min_linear_velocity", min_linear_velocity_, -2.0);
  private_nh_.param("min_angular_velocity", min_angular_velocity_, -2.0);
  private_nh_.param("start_delimitter", start_delimitter_, std::string("["));
  private_nh_.param("end_delimitter", end_delimitter_, std::string("]"));
  private_nh_.param("pub_topic", pub_topic_, std::string("cmd_vel"));
}

void RadioController::run() {
  ros::Rate loop_rate(rate_);
  auto last_update_time = ros::Time::now();
  this->serial_parser = new serial::SerialParser(*serial_, start_delimitter_,
                                                 end_delimitter_, data_size_);
  std::string parsed_string;
  auto *buffer = (uint8_t *)malloc(sizeof(uint8_t) * data_size_);
  auto buffer_length = (size_t)data_size_;
  ros::Publisher pub = nh_->advertise<geometry_msgs::Twist>(pub_topic_, 1000);
  geometry_msgs::Twist msg;
  float linear, angular, old_linear = 0, old_angular = 0;
  while (ros::ok()) {
    parsed_string = this->serial_parser->get_parsed_string();
    if (!parsed_string.empty()) {
      angular = (float)parsed_string[0] -'0' ;
      linear = (float)parsed_string[1] - '0';

      linear = map(linear, min_linear_velocity_, max_linear_velocity_);
      angular = map(angular, min_angular_velocity_, max_angular_velocity_);

      linear = 0.8 * old_linear + 0.2*linear;
      angular = 0.8* old_angular + 0.2*angular;

      old_linear = linear;
      old_angular = angular;

      msg.linear.x = linear;
      msg.angular.z = angular;
      pub.publish(msg);
    }
    ros::spinOnce();
    loop_rate.sleep();
  }
}
