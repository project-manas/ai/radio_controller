/* Project Manas
 * FS-CT6B pwm input
 * 4 bit data transmission over FTDI
Author : Ritwik Agarwal*/

int ch1 = A0;
int ch3 = A2;
int ch5 = A4;
int ch6 = A5;
int LED_PIN = 8;
int state = 0;
long long int now= 0 ;

#include <SoftwareSerial.h>

SoftwareSerial usb (9,10);
#define MINIMUM 0
#define MAXIMUM 8

void setup() {
  usb.begin(9600);
  Serial.begin(9600);
  pinMode(ch1,INPUT);
  pinMode(ch3,INPUT);
  pinMode(ch5,INPUT);
  pinMode(ch6,INPUT);
  pinMode(LED_PIN,OUTPUT);
}

void loop() {
  // read the values
  long raw_angular, raw_linear,switch_l,switch_r;
  raw_angular = pulseIn(ch1, HIGH);
  raw_linear = pulseIn(ch3, HIGH);
  switch_r = pulseIn(ch5, HIGH);
  switch_l = pulseIn(ch6, HIGH);
  //Serial.print(raw_angular);
  //Serial.print("\t");
  //Serial.println(raw_linear);
  //map the values

  switch_l = map(switch_l, 997, 1400, 0, 1);
  switch_r = map(switch_r, 996, 1400, 0, 1);

  if(raw_angular != 0 && raw_linear != 0){
    raw_angular = map(raw_angular, 1085, 1760, MINIMUM, MAXIMUM);
    raw_linear = map(raw_linear, 1120, 1870, MINIMUM, MAXIMUM);

    //clamping the values
    if(raw_angular < MINIMUM)
     raw_angular = MINIMUM;
    if(raw_linear < MINIMUM)
      raw_linear = MINIMUM;
    if(raw_angular > MAXIMUM)
      raw_angular = MAXIMUM;
    if(raw_linear > MAXIMUM)
      raw_linear = MAXIMUM;

    if(switch_r){
      //USB write 4-bit
      usb.write("[");
      usb.print(raw_angular);
      usb.print(raw_linear);
      usb.print(switch_r);
      usb.print(switch_l);
      usb.write("]");
      digitalWrite(LED_PIN,HIGH);
    }
    else{
      if((millis()-now) > 500){
        now = millis();
        if(state == 1){
          digitalWrite(LED_PIN,LOW);
          state = 0;
        }
        else if(state == 0){
          digitalWrite(LED_PIN,HIGH);
          state = 1;
        }
      }
    }
    //Serial write
    /*Serial.print(raw_angular);
    Serial.print('\t');
    Serial.print(raw_linear);
    Serial.print('\t');
    Serial.print(switch_l);
    Serial.print('\t');
    Serial.print(switch_r);
    Serial.println();*/
  }
}
